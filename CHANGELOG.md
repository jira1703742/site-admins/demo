# [1.1.0](https://gitlab.com/jira1703742/site-admins/demo/compare/1.0.0...1.1.0) (2023-10-17)


### Features

* **GTMS-20:** Create FAQs ([59ffbf7](https://gitlab.com/jira1703742/site-admins/demo/commit/59ffbf7c43d590d16d515366ca0b31f809acbc98))

# 1.0.0 (2023-10-17)


### Features

* sematic release ([6a15618](https://gitlab.com/jira1703742/site-admins/demo/commit/6a15618aa8dc586936613cebb14a141c95e20f66))
